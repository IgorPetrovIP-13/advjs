class Emplyee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age; 
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(newName) {
        this._name = newName;
    }

    get age() {
        return this._age;
    }

    set age(newAge) {
        this._age = newAge;
    }
    
    get salary() {
        return this._salary;
    }
    
    set salary(newSalary) {
        this._salary = newSalary;
    }
}

class Programmer extends Emplyee {
    constructor(name, age, salary, lang = ['English']) {
        super(name, age, salary)
        this._lang = lang;
    }

    get lang() {
        return this._lang;
    }
    
    set lang(newLang) {
        this._lang = newLang;
    }

    get salary () {
        return this._salary * 3;
    }
}


const emp = new Emplyee("Igor", 19, 500);
const prg1 = new Programmer("Igor", 19, 500);
const prg2 = new Programmer("Evgen", 19, 700, ['English', 'German']);

console.log(emp, prg1, prg2);
console.log(emp.salary, prg1.salary, prg2.salary);