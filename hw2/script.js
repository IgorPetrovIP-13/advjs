const rootDiv = document.getElementById('root');

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

function createUl(){
  const outerUl = document.createElement('ol');
  let innerUl;
  rootDiv.appendChild(outerUl);
  for (let i in books) {
    if (isValid(books[i], i)) {
      outerUl.appendChild(document.createElement('li'));
      innerUl = document.createElement('ul');
      outerUl.lastChild.textContent = `Position in list ${i}`
      outerUl.lastChild.appendChild(innerUl);
      innerUl.innerHTML = `<li>Author: ${books[i].author}</li><li>Name: ${books[i].name}</li><li>Price: ${books[i].price}</li>`
    }
  }
}

function isValid(book, index) {
  let flag = true;
  if (!book.hasOwnProperty('author')) {
    console.log(`book № ${index} has no property 'author'`)
    flag = false;
  }
  else if (!book.hasOwnProperty('name')){
    console.log(`book № ${index} has no property 'name'`)
    flag = false;
  }
  else if (!book.hasOwnProperty('price')){
    console.log(`book № ${index} has no property 'price'`)
    flag = false;
  }
  return flag;
}

createUl();