const dataLink = "https://ajax.test-danit.com/api/swapi/films";
const filmsContainer = document.querySelector('.films-container');

function getData(url) {
  return fetch(url).then(response => response.json());
}

function getCharactersInfo(characters) {
  return Promise.all(characters.map((character) => {
    return getData(character).then(data => data.name)
  }))
}

function main() {
  getData(dataLink).then((data) => {
    data.sort((a, b) => {
      if (a.episodeId > b.episodeId) {
        return 1;
      }
      if (a.episodeId < b.episodeId) {
        return -1;
      }
      return 0;
    })
    for (film of data) {
      const filmElement = document.createElement('div');
      filmElement.innerHTML = `
      <h2>Episode ${film.episodeId}: ${film.name}</h2>
      <p>${film.openingCrawl}</p>
      <div class="lds-dual-ring"></div>
      `;
      filmsContainer.appendChild(filmElement);
      getCharactersInfo(film.characters)
      .then(characters => {
        const characterList = document.createElement('ul');
        for(character of characters) {
          const characterLi = document.createElement('li');
          characterLi.textContent = character;
          characterList.appendChild(characterLi);
        }
        filmElement.replaceChild(characterList, document.querySelector('.lds-dual-ring'));
      })
      .catch(e => {
        console.log(`Error occured while downloading character list: ${e}`)
      }) 
    }
  })
  .catch(e => {
    console.log(`Error occured while downloading film list: ${e}`)
  })
}

main();