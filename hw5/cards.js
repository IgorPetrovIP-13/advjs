const usersUrl = 'https://ajax.test-danit.com/api/json/users';
const postsUrl = 'https://ajax.test-danit.com/api/json/posts';

class Card {
    #id
    #userName;
    #userNick;
    #userEmail;
    #postTitle;
    #postText;

    constructor(post,user) {
        this.#id = post.id;
        this.#userName = user.name;
        this.#userNick = user.username;
        this.#userEmail = user.email;
        this.#postTitle = post.title;
        this.#postText = post.body;
    }

    createCardBody() {
        this.rootDiv = document.createElement('div');
        this.rootDiv.classList.add('post');

        this.nameHeader = document.createElement('h1');
        this.nameHeader.classList.add('post__name');
        this.nameHeader.textContent = this.#userName;

        this.nickHeader = document.createElement('h3');
        this.nickHeader.classList.add('post__nick');
        this.nickHeader.textContent = "@" + this.#userNick;

        this.emailHeader = document.createElement('h4');
        this.emailHeader.classList.add('post__email');
        this.emailHeader.textContent = this.#userEmail;

        this.rootDiv.appendChild(document.createElement('div'));

        this.userImg = document.createElement('img');
        this.userImg.src = "https://th.bing.com/th/id/OIG.lVXjWwlHyIo4QdjnC1YE";
        this.userImg.alt = "user-photo";
        this.rootDiv.lastChild.classList.add('post__header-div');
        this.rootDiv.lastChild.append(this.userImg, this.nameHeader, this.nickHeader, this.emailHeader);

        this.postHeader = document.createElement('h2');
        this.postHeader.classList.add('post__title')
        this.postHeader.textContent = this.#postTitle;

        this.postText = document.createElement('p');
        this.postText.classList.add('post__text')
        this.postText.textContent = this.#postText;

        this.rootDiv.appendChild(document.createElement('div'));
        this.rootDiv.lastChild.classList.add('post__text-div');
        this.rootDiv.lastChild.append(this.postHeader, this.postText);

        this.deleteButton = document.createElement('button');
        this.deleteButton.classList.add('delete-button');
        this.deleteButton.textContent = 'DELETE POST';
        this.rootDiv.appendChild(this.deleteButton);

        this.addImgListener();
        this.addButtonListener();

        document.body.prepend(this.rootDiv);
    };

    addImgListener() {
        let imageLink;
        this.userImg.addEventListener('click', () => {
            imageLink = prompt('New Image Link');
            fetch(imageLink)
            .then((response) => {
                if(response.ok) {
                    fetch(postsUrl + `/${this.#id}`, { 
                        method: 'PUT',
                        headers: {
                            'Content-type': 'application/json; charset=UTF-8'
                        },
                        body: JSON.stringify({
                            'imagelink': imageLink
                        })
                    })
                    .then(response => {
                        if (response.ok) {
                            this.userImg.src = imageLink;
                        }
                        else {
                            throw new Error('Error while PUT');
                        }
                    })
                    .catch(e => {
                        console.log(e);
                    })
                }
                else {
                    throw new Error('Bad link');
                }
            })
            .catch(e => {
                console.log(e);
            })
        })
    };

    addButtonListener() {
        this.deleteButton.addEventListener('click', ()=> {
            fetch(postsUrl + `/${this.#id}`, {
                method: 'DELETE'
            })
            .then((response)=>{
                if (response.ok){
                    this.rootDiv.remove();
                }
                else {
                    throw new Error('Error while deleting this post!')
                }
            })
            .catch(e => {
                console.log(e);
            })
        })
    }
}

function getData(url) {
    return fetch(url).then(response => response.json());
}
 
function showPosts() {
    Promise.all([getData(usersUrl), getData(postsUrl)])
    .then (data => {
            const allUsers = data[0];
            const allPosts = data[1];
            let user;
            for (let post of allPosts) {
                user = allUsers.find(user => user.id === post.userId);
                if (user) {
                    new Card(post, user).createCardBody();
                }
            }
        }
    )
    .catch (e => {
        console.log(e);
    })
}

showPosts();