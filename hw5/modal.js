class ModalWin {

  #openBtn;

  constructor(btn) {
    this.#openBtn = btn;
  }

  addModal() {
    this.mainWin = document.createElement("div");
    this.mainWin.classList.add('modal');
    this.modalContent = document.createElement("div");
    this.modalContent.classList.add('modal-content');
    this.titleInput = document.createElement('input');
    this.titleInput.type = 'text';
    this.titleInput.placeholder = "Title";
    this.textArea = document.createElement('textarea');
    this.textArea.placeholder = "Post text";
    this.textArea.rows ="4";
    this.textArea.cols="30";
    this.submitButton = document.createElement("button");
    this.submitButton.innerText = 'Submit';
    this.closeButton = document.createElement("button");
    this.closeButton.innerText = 'Close';
    this.modalContent.append(this.titleInput, this.textArea, this.submitButton, this.closeButton);
    this.mainWin.appendChild(this.modalContent);
    this.closeNOpenListeners();
    this.addSubmitListener();
    document.body.appendChild(this.mainWin);
  }

  addSubmitListener() {
    this.submitButton.addEventListener('click', () => {
      if(this.titleInput.value && this.textArea.value) {
        this.mainWin.style.display = "none";
        fetch(postsUrl, {
          method: "POST",
          body: JSON.stringify({
            userId: 1,
            title: this.titleInput.value,
            body: this.textArea.value
          }),
          headers: {
            "Content-type": "application/json; charset=UTF-8"
          }
        })
        .then(response => {
          if(response.ok) {
            return response.json()
          }
          else {
            throw new Error('Error');
          }
        })
        .then(data => {
          getData(usersUrl)
          .then(userData => {
            new Card(data, userData[0]).createCardBody();
          })
          .catch(e => {
            console.log(e);
          })
        })
        .catch(e => {
          console.log(e);
        })
      }
    })
  }

  closeNOpenListeners() {
    this.closeButton.addEventListener('click', ()=> {
      this.mainWin.style.display = "none";
    })

    this.#openBtn.addEventListener('click', ()=> {
      this.mainWin.style.display = "block";
    })
  }
}

const modal = new ModalWin(document.querySelector('#open-modal'));
modal.addModal();