document.querySelector('#ip-btn')?.addEventListener('click', async function() {
    try {
        const getIp = await fetch('https://api.ipify.org/?format=json');
        if (getIp.ok) { 
            const ip = (await getIp.json()).ip;
            const getInfo = await fetch(`http://ip-api.com/json/${ip}`);
            if (getInfo.ok) {
                const info = await getInfo.json();
                document.querySelector('.info').textContent = `Continent: ${info.timezone.split('/')[0]}; Country: ${info.country};
                Region: ${info.regionName}; City: ${info.city}; District: ${info.district || 'NoDistrict'}`
            }else {throw new Error('Error');}
        }else {throw new Error('Error');}
    }
    catch(e) {
        console.log(e);
    }
})